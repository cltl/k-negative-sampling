'''
Created on Dec 18, 2014

@author: Minh Ngoc Le
'''
from collections import defaultdict
import itertools
import os
import sys
from nltk.corpus import wordnet as wn
from similarity import LemmaPos2WordNetAdapter, wup_similarity
from eval import _safe_spearmanr, _safe_pearson, threshold
import numpy as np
from math import floor
from eval import ordering
from operator import itemgetter
from eval.ordering import accuracy, score_with_tie_correction, score_naive
from scipy.stats import rankdata


home_dir = os.path.dirname(__file__)
data_path = os.path.join(home_dir, 'SimLex-999.txt')
assert wn.get_version() == '3.0'

data = None
data_by_pos = None
data_high_assoc = None
data_low_assoc = None

def _init():
    global data, data_by_pos, data_high_assoc, data_low_assoc
    if data and data_by_pos:
        return
    data = []
    data_high_assoc = []
    data_low_assoc = []
    with open(data_path) as f:
        sys.stderr.write('Reading SimLex-999 dataset from %s... ' %data_path)
        f.readline() # skip headders
        for line in f:
            fields = line.strip().split('\t')
            dp = (fields[0], fields[1], fields[2].lower(), float(fields[3]))
            data.append(dp)
            if fields[8] == '1': data_high_assoc.append(dp)
            else: data_low_assoc.append(dp)
        sys.stderr.write('Done\n')
    data_by_pos = defaultdict(list)
    for point in data:
        data_by_pos[point[2]].append(point)
    return data, data_by_pos


def get_lemma_and_pos():
    _init()
    ret = set()
    for lemma1, lemma2, pos, _ in data:
        ret.add((lemma1, pos))
        ret.add((lemma2, pos)) 
    return ret


def _evalute_on_dataset(sim, data):
    _, _, _, gold = zip(*data)
    predicted = [sim((lemma1, pos), (lemma2, pos)) 
                 for lemma1, lemma2, pos, _ in data]
    return _safe_spearmanr(predicted, gold)


def evaluate(sim):
    _init()
    return _evalute_on_dataset(sim, data)

# 
# def evaluate(sim):
#     """
#     Evaluate sim by calling it on 999 pairs of words in SimLex.
#     Each word is represented by a tuple (lemma, pos).
#     Return a list of  results on the dataset and 3 subsets of it.
#     """
#     _init()
#     return ([['all'] + list(_evalute_on_dataset(sim, data))] +
#             [[pos] + list(_evalute_on_dataset(sim, data_by_pos[pos])) 
#              for pos in sorted(data_by_pos)])
# 
# 
# def evaluate_and_print(sim):
#     print "Spearman's correlation with SimLex-999:"
#     for part, score, num in evaluate(sim):
#         print "%s\t%.4f\t(%d pairs)" %(part, score, num)
#     sys.stdout.flush()


def _filter_data_by_pos(data, pos):    
    return [dp for dp in data if dp[2] in pos]


# def evaluate_and_print_high_assoc_nv(sim):
#     _init()
#     data_high_assoc_nv = _filter_data_by_pos(data_high_assoc, ('n', 'v'))
#     print
#     print "Size of SimLex-999(assoc,nv): %d" %len(data_high_assoc_nv)
#     _, _, _, gold = zip(*data_high_assoc_nv)
#     predicted = [sim((lemma1, pos), (lemma2, pos)) 
#                  for lemma1, lemma2, pos, _ in data_high_assoc_nv]
#     print ("Ordering accuracy on SimLex-999(assoc,nv): %.4f (%d pairs)" 
#            %accuracy(gold, predicted))
#     print ("Spearman's correlation on SimLex-999(assoc,nv): %.4f (%d pairs)" 
#            %_safe_spearmanr(predicted, gold))
#     print ("Pearson correlation on SimLex-999(assoc,nv): %.4f (%d pairs)" 
#            %_safe_pearson(predicted, gold))
# #     print "\n".join("%.2f\t%.2f" %dp for dp in zip(predicted, gold))
#     sys.stdout.flush()
# 
# 
# def evaluate_and_print_low_assoc_nv(sim):
#     _init()
#     data_low_assoc_nv = _filter_data_by_pos(data_low_assoc, ('n', 'v'))
#     print "Size of SimLex(assoc,nv): %d" %len(data_low_assoc_nv)
#     _, _, _, gold = zip(*data_low_assoc_nv)
#     predicted = [sim((lemma1, pos), (lemma2, pos)) 
#                  for lemma1, lemma2, pos, _ in data_low_assoc_nv]
#     print ("Ordering accuracy on SimLex(assoc,nv): %.4f (%d pairs)" 
#            %accuracy(gold, predicted))
#     print ("Spearman's correlation on SimLex(assoc,nv): %.4f (%d pairs)" 
#            %_safe_spearmanr(predicted, gold))
#     print ("Pearson correlation on SimLex(assoc,nv): %.4f (%d pairs)" 
#            %_safe_pearson(predicted, gold))
# #     print "\n".join("%.2f\t%.2f" %dp for dp in zip(predicted, gold))
#     sys.stdout.flush()

